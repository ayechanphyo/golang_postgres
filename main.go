package main

import (
	"fmt"
	"go-postgres/router"
	"log"
	"net/http"
)

func main() {
	r := router.Router()
	// fs := http.FileServer(http.Dir("build"))
	// http.Handle("/", fs)
	fmt.Println("Starting server on the port 8080...")
	// err := http.ListenAndServe(GetPort(), nil)
	// 14 	if err != nil {
	// 15 		log.Fatal("ListenAndServe: ", err)
	// 16 	}
	log.Fatal(http.ListenAndServe(":8080", r))
}
