module go-postgres

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/joho/godotenv v1.3.0
	github.com/kr/fs v0.1.0 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/lib/pq v1.6.0
	github.com/tools/godep v0.0.0-20180126220526-ce0bfadeb516 // indirect
	golang.org/x/tools v0.0.0-20200616133436-c1934b75d054 // indirect
)
